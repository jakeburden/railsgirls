# RailsGirls Example app

Basic Rails app created by following the introductory [RailsGirls](http://guides.railsgirls.com/app) steps.

For deployment with [GitLab AutoDevops](https://docs.gitlab.com/ee/topics/autodevops/).
